package JITSvilleTravelGroups.JITSvilleTravelGroups;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VacationPassangerTest{
	VacationPassanger vacationPassangerObj1;
	VacationPassanger vacationPassangerObj2;
	//int mealCount=0;
	
	@Before
	public void setupPassangerInfo(){
		
		/* vacationPassangerObj1 = new VacationPassanger(90,1,false);
		 vacationPassangerObj2 = new VacationPassanger(199,2,true);
		 
		 VacationPassanger VacationPassangerObj= new VacationPassanger();
			
		 //add Passengers in the list
		 VacationPassangerObj.addPassangerDetails(vacationPassangerObj1);
		 VacationPassangerObj.addPassangerDetails(vacationPassangerObj2);
		 
		 mealCount=VacationPassangerObj.getMealCount();
		 System.out.println("mealCount in setupPassangerInfo is-->"+mealCount);
		 
		 //System.out.println(VacationPassangerObj.getPassangerListCount());
		*/ 
		 
	}
	
	/*@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void negativeMilesTest(){
		 
		  exception.expect(IllegalArgumentException.class);
		  exception.expectMessage("error");
		  vacationPassangerObj2 = new VacationPassanger(-3,true);
	}
	*/
	 
	

	/*@Test
    public void vacationPassangerSizeTest() {
		VacationPassanger vacationPassangerObj1= new VacationPassanger();
		System.out.println("List count??????????? **********"+vacationPassangerObj1.getListCount());
	 assertEquals(0, vacationPassangerObj1.getListCount());
}*/
	
	@Test(expected = IllegalArgumentException.class)
    public void testDivisionWithException() {
	 vacationPassangerObj2 = new VacationPassanger(-3,1,true);
}
	
	@Test(expected = IllegalArgumentException.class)
    public void milesShouldNotBegreaterTest() {
	 vacationPassangerObj2 = new VacationPassanger(25000,2,true);
	}
	
	@Test
	public void totalNoOfMealsTest() {
		vacationPassangerObj1 = new VacationPassanger(90,1,false);
     	vacationPassangerObj2 = new VacationPassanger(199,2,true);
     	
     	VacationPassanger vacationPassanger = new VacationPassanger(); 
		System.out.println("mealCount in totalNoOfMealsTest is-->"+vacationPassanger.getMealCount());
		
		assertEquals(9.0,vacationPassanger.getMealCount(),0.001);
	}
	
	
}
