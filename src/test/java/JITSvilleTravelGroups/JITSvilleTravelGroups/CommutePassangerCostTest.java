package JITSvilleTravelGroups.JITSvilleTravelGroups;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class CommutePassangerCostTest {
	
	CommutePassanger commutePassangerObj1;
	CommutePassanger commutePassangerObj2;
	CommutePassanger commutePassangerObj3;
	
	CommutePassanger commutePassangerObj= new CommutePassanger();
	int passangersCount=0;
	private List<CommutePassanger> commutePassangerList= new LinkedList<CommutePassanger>();

	@Before
	public void setupCommutePassangers(){
		 commutePassangerObj1 = new CommutePassanger(3,true,false);
		 commutePassangerObj2 = new CommutePassanger(5,true,false);
		 commutePassangerObj3 = new CommutePassanger(4,false,true);
		  	
		 //add passengers in the list
		 commutePassangerObj.addCommutePassangerDetails(commutePassangerObj1);
		 commutePassangerObj.addCommutePassangerDetails(commutePassangerObj2);
		 commutePassangerObj.addCommutePassangerDetails(commutePassangerObj3);
		 
		 commutePassangerList.add(commutePassangerObj1);
		 commutePassangerList.add(commutePassangerObj2);
		 commutePassangerList.add(commutePassangerObj3);
		 
		 passangersCount=commutePassangerObj.getCommutePassangerList().size();
		 System.out.println("passangersCount-->"+passangersCount);
	}
	
	@Test
	public void calculateFairWithoutRiderCard() {
		CommutePassanger commutePassanger = new CommutePassanger(3,true,false);
		
		double totalCost=new CommutePassangerCost().calculateFair(commutePassanger);
		System.out.println("Total Cost is-->"+totalCost);
		
		assertEquals(1.35, totalCost,0.001);
	}
	
	@Test
	public void calculateFairWithoutRiderCardForSecondTest() {
		CommutePassanger commutePassanger = new CommutePassanger(5,true,false);
		
		double totalCost=new CommutePassangerCost().calculateFair(commutePassanger);
		System.out.println("Total Cost is******-->"+totalCost);
		
		assertEquals(2.25, totalCost,0.001);
	}
	
	@Test
	public void noFrequentRiderButHaveNewsPaperTest() {
		CommutePassanger commutePassanger = new CommutePassanger(4,false,true);
		
		double totalCost=new CommutePassangerCost().calculateFair(commutePassanger);
		System.out.println("Total Cost for noFrequentRiderButHaveNewsPaperTest is******-->"+totalCost);
		
		assertEquals(2.0, totalCost,0.001);
	}
	
	
	
}
