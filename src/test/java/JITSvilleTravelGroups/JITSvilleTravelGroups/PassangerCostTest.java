/**
 * 
 */
package JITSvilleTravelGroups.JITSvilleTravelGroups;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

/**
 * @author gyelwand
 *
 */
public class PassangerCostTest {
	CommutePassangerCost commutePassangerCostObj;
	VacationPassangerCost vacationPassangerCostObj;
	PassengerCost passangerCostObj; 
	
	@Before
	public void initializeData() {
		
		/*VacationPassanger vacationPassangerObj1 = new VacationPassanger(90,1,false);
		VacationPassanger vacationPassangerObj2 = new VacationPassanger(199,2,true);
		
		CommutePassanger commutePassangerObj1 = new CommutePassanger(3,true,false);
		CommutePassanger commutePassangerObj2 = new CommutePassanger(5,true,false);
		CommutePassanger commutePassangerObj3 = new CommutePassanger(4,false,true);
		*/
		/*double commutePassangersFair1=commutePassangerCostObj.calculateFair(commutePassangerObj1);
		double commutePassangersFair2=commutePassangerCostObj.calculateFair(commutePassangerObj2);
		double commutePassangersFair3=commutePassangerCostObj.calculateFair(commutePassangerObj3);
		
		double vacationPassangerFair1=vacationPassangerCostObj.calculateFair(vacationPassangerObj1);
		double vacationPassangerFair2=vacationPassangerCostObj.calculateFair(vacationPassangerObj1);
		
		double finalFair=vacationPassangerFair1 +vacationPassangerFair2+commutePassangersFair1+commutePassangersFair2+commutePassangersFair3;
		
		double totalNoOfMealsNeaded=0;
		double totalNoOfPapersNeeded=0;*/
		
	}
	
	@Test
	public void totalNoOfPapersTest() {
		//System.out.println("in totalNoOfPapersTest-->");
		VacationPassanger vacationPassangerObj1 = new VacationPassanger(90,1,false);
		VacationPassanger vacationPassangerObj2 = new VacationPassanger(199,2,true);
		
		CommutePassanger commutePassangerObj1 = new CommutePassanger(3,true,false);
		CommutePassanger commutePassangerObj2 = new CommutePassanger(5,true,false);
		CommutePassanger commutePassangerObj3 = new CommutePassanger(4,false,true);
		
		CommutePassanger commuteObj= new CommutePassanger();
		VacationPassanger vacationObj= new VacationPassanger();
		
		int commutePassengerPaperCount=commuteObj.getPaperCount();
		int vacationPassengerPaperCount=vacationObj.getPaperCount();
		int totalPaperCount=commutePassengerPaperCount+vacationPassengerPaperCount;
		
		
		int totalNoOfPapers=new PassengerCost().calculateTotalNoOfPapers();
		System.out.println("Finally got it-->"+totalNoOfPapers);
		
		//assertEquals(2,totalNoOfPapers,0.01);
	}
	
	@Test
	public void totalCostCalculatorTest() {
		
		CommutePassangerCost commuteObj= new CommutePassangerCost();
		VacationPassangerCost vacationObj= new VacationPassangerCost();
		System.out.println("in totalCostCalculatorTest-->");
		VacationPassanger vacationPassangerObj1 = new VacationPassanger(90,1,false);
		VacationPassanger vacationPassangerObj2 = new VacationPassanger(199,2,true);
		
		CommutePassanger commutePassangerObj1 = new CommutePassanger(3,true,false);
		CommutePassanger commutePassangerObj2 = new CommutePassanger(5,true,false);
		CommutePassanger commutePassangerObj3 = new CommutePassanger(4,false,true);
		System.out.println("Ganga");
		double commutePassangersFair1=commuteObj.calculateFair(commutePassangerObj1);
		//System.out.println("commutePassangersFair1------------->"+commutePassangersFair1);
		double commutePassangersFair2=commuteObj.calculateFair(commutePassangerObj2);
		double commutePassangersFair3=commuteObj.calculateFair(commutePassangerObj3);
		
		double vacationPassangerFair1=vacationObj.calculateFair(vacationPassangerObj1);
		double vacationPassangerFair2=vacationObj.calculateFair(vacationPassangerObj2);
		
		//double finalFair=vacationPassangerFair1 +vacationPassangerFair2+commutePassangersFair1+commutePassangersFair2+commutePassangersFair3;
		double finalFair=vacationPassangerFair1+vacationPassangerFair2+commutePassangersFair1+commutePassangersFair2+commutePassangersFair3;
		
		System.out.println("final fair--->"+finalFair);
		
		
		double totalCostForAllPassangers=new PassengerCost().calculateTotalCost();
		System.out.println("totalCostForAllPassangers--->"+totalCostForAllPassangers);
	
		assertEquals(154.0,totalCostForAllPassangers, 0);
	}

}
