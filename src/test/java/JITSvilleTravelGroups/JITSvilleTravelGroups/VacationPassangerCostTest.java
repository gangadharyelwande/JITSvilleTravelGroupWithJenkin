package JITSvilleTravelGroups.JITSvilleTravelGroups;

import org.junit.Test;

public class VacationPassangerCostTest{

    VacationPassanger vacationPassanger;
    
	@Test(expected = IllegalArgumentException.class)
    public void testDivisionWithException() {
		vacationPassanger = new VacationPassanger(3,0,true);
	}
	
	@Test(expected = IllegalArgumentException.class)
    public void invalidMealsProvidedTest() {
		vacationPassanger = new VacationPassanger(76,2,true);
	}
}

