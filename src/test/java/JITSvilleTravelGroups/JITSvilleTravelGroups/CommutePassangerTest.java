/**
 * 
 */
package JITSvilleTravelGroups.JITSvilleTravelGroups;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


/**
 * @author gyelwand
 *
 */
public class CommutePassangerTest {

	CommutePassanger commutePassangerObj1;
	CommutePassanger commutePassangerObj2;
	CommutePassanger commutePassangerObj3;
	
	CommutePassanger commutePassangerObj= new CommutePassanger();
	int passangersCount=0;
	private List<CommutePassanger> commutePassangerList= new LinkedList<CommutePassanger>();
	
	@Before
	public void setupCommutePassangerInfo(){
		 commutePassangerObj1 = new CommutePassanger(3,true,false);
		 commutePassangerObj2 = new CommutePassanger(5,true,false);
		 commutePassangerObj3 = new CommutePassanger(4,false,true);
		  	
		 //add passengers in the list
		 commutePassangerObj.addCommutePassangerDetails(commutePassangerObj1);
		 commutePassangerObj.addCommutePassangerDetails(commutePassangerObj2);
		 commutePassangerObj.addCommutePassangerDetails(commutePassangerObj3);
		 
		 commutePassangerList.add(commutePassangerObj1);
		 commutePassangerList.add(commutePassangerObj2);
		 commutePassangerList.add(commutePassangerObj3);
		 
		 passangersCount=commutePassangerObj.getCommutePassangerList().size();
		 System.out.println("passangersCount-->"+passangersCount);
	}
	
	@Test
    public void commutePassangerSizeTest() {
		System.out.println("In commutePassangerSizeTest-->"+passangersCount);
		assertEquals(3.0, passangersCount,0.001);
    }
	
	@Test(expected = IllegalArgumentException.class)
    public void negativeStopTest() {
		System.out.println("In negativeStopTest-->");
		commutePassangerObj1 = new CommutePassanger(-5,true,false);
	}
}
