package JITSvilleTravelGroups.JITSvilleTravelGroups;

public class CommutePassangerCost implements PassangerCategory{
	
	double rateFactor=0.5;
	private static int finalFair;
	
	public double calculateFair(CommutePassanger passangerInfo) {
		int noOfStops=passangerInfo.getStops();
		boolean hasFrequentRiderCard=passangerInfo.isFrequentCard();
		
		double totalFairPerPassanger=rateFactor*noOfStops;
		
		if(hasFrequentRiderCard) {
			double discount=totalFairPerPassanger*0.1;
			System.out.println("discount is:"+discount);
			totalFairPerPassanger=totalFairPerPassanger-discount;
		}
		finalFair+=totalFairPerPassanger;
		return totalFairPerPassanger;
	}

	public double calculateFair() {
		return finalFair;
	}


}
