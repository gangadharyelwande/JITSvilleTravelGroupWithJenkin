package JITSvilleTravelGroups.JITSvilleTravelGroups;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class VacationPassanger implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5512693843849799778L;
	private int miles;
	private boolean newsPaperNeeded;
	private  int listCount=0;
	private int meals;
	
	private  static int totalNoOfMeals=0;
	private  int totalNoOfPapers;
	
	
	/**
	 * @return the meals
	 */
	public int getMeals() {
		return meals;
	}

	/**
	 * @param meals the meals to set
	 */
	public void setMeals(int meals) {
		this.meals = meals;
	}
	private List<VacationPassanger> vacationPassList= new LinkedList<VacationPassanger>();
	
	
	public void addPassangerDetails(VacationPassanger vacationPassanger) {
		this.vacationPassList.add(vacationPassanger);
		
	}
	
	public VacationPassanger() {
		
	}
	public VacationPassanger(int miles, int meals, boolean newsPaperNeeded) throws IllegalArgumentException{
		
		if(miles<100 && meals>1) {
			throw new IllegalArgumentException("Invalid No Of Meals Served");
		}
		if(miles<0) {
			throw new IllegalArgumentException("error");
		}
		
		if(miles>4000) {
			throw new IllegalArgumentException("Miles should not be greater than 4000");
		}
		
		if(miles<5) {
			throw new IllegalArgumentException("Illegal Argument Exception");
		}
		
		this.miles=miles;
		this.meals=meals;
		this.newsPaperNeeded=newsPaperNeeded;
	
		if(newsPaperNeeded) {
			totalNoOfPapers=totalNoOfPapers+1;
		}
		int totalMeals=(miles/100)+1;

		totalNoOfMeals=totalNoOfMeals+totalMeals;
		
	}
	
	public int getMealCount() {
		//int tempValue=totalNoOfMeals;
		//VacationPassanger.totalNoOfMeals=0;
		return totalNoOfMeals;
	}
	
	public int getPaperCount() {
		return totalNoOfPapers;
	}
	
	
	/**
	 * @return the listCount
	 */
	public  int getListCount() {
		return listCount;
	}
	/**
	 * @param listCount the listCount to set
	 */
	public  void setListCount(int listCount) {
		this.listCount = listCount;
	}
	/**
	 * @return the vacationPassList
	 */
	public  List<VacationPassanger> getVacationPassList() {
		return vacationPassList;
	}
	
	/**
	 * @param vacationPassList the vacationPassList to set
	 */
	public void setVacationPassList(List<VacationPassanger> vacationPassList) {
		this.vacationPassList = vacationPassList;
	}
	
	//Add Passengers details in the list
	public int getPassangerListCount() {
		this.listCount= vacationPassList.size();
		return listCount;
		
	}
	
	/**
	 * @return the miles
	 */
	public int getMiles() {
		return miles;
	}
	/**
	 * @param miles the miles to set
	 */
	public void setMiles(int miles) {
		this.miles = miles;
	}
	/**
	 * @return the newsPaperNeeded
	 */
	public boolean isNewsPaperNeeded() {
		return newsPaperNeeded;
	}
	/**
	 * @param newsPaperNeeded the newsPaperNeeded to set
	 */
	public void setNewsPaperNeeded(boolean newsPaperNeeded) {
		this.newsPaperNeeded = newsPaperNeeded;
	}
}
