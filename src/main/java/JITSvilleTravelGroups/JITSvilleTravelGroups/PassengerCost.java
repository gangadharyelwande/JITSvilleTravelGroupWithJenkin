package JITSvilleTravelGroups.JITSvilleTravelGroups;

public class PassengerCost extends TravelCostCalculator{
    
	private static double finalTotalAmount;
	PassengerCost(PassangerCategory passCategory){
		super(passCategory);
	}
	
	public PassengerCost() {
		super();
	}

	public double calculateTotalCost(PassangerCategory passangerCategory) {
		double totalForCommutePassengers=0.0;
		double totalForVacationPassengers=0.0;
				
		    if(passangerCategory instanceof CommutePassangerCost) {
		    	CommutePassangerCost commutePass = new CommutePassangerCost();
		    	totalForCommutePassengers=commutePass.calculateFair();
		    	finalTotalAmount=finalTotalAmount+commutePass.calculateFair();
		    }
		    
		    if(passangerCategory instanceof VacationPassangerCost) {
		    	VacationPassangerCost vacationPass = new VacationPassangerCost();
		    	totalForVacationPassengers=vacationPass.calculateFair();
		    	finalTotalAmount=finalTotalAmount+vacationPass.calculateFair();
		    }
			
			double finalTotal=	totalForCommutePassengers + totalForVacationPassengers;
			System.out.println("Final cost of all passangers is-->"+finalTotal);
			System.out.println("finalTotalAmount of all passangers is-->"+finalTotalAmount);
	        
	        return finalTotalAmount;
	}
	
	public int calculateTotalNoOfPapers() {
		System.out.println("hellooo");
		CommutePassanger commutePass = new CommutePassanger();
		VacationPassanger vacationPass = new VacationPassanger();
		
		int totalNoOfPapers=commutePass.getPaperCount()+vacationPass.getPaperCount();
		
		System.out.println("totalNoOfPapers in PassengerCost is:"+totalNoOfPapers);
		
		return totalNoOfPapers;
	}
	
	public double calculateTotalCost() {
		CommutePassangerCost commutePassCost = new CommutePassangerCost();
		VacationPassangerCost vacationPassCost = new VacationPassangerCost();
		
		double totalCostForAllPassangers=commutePassCost.calculateFair()+vacationPassCost.calculateFair();
		return totalCostForAllPassangers;
	}
}
