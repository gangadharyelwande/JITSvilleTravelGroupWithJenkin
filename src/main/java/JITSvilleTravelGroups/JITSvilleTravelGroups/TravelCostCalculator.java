/**
 * 
 */
package JITSvilleTravelGroups.JITSvilleTravelGroups;

/**
 * @author gyelwand
 *
 */
public abstract class TravelCostCalculator {
	
	protected PassangerCategory passangerCategory; 
	
	protected TravelCostCalculator() {
		
	}
	protected TravelCostCalculator(PassangerCategory passCategory) {
		this.passangerCategory=passCategory;
	}
	
	public abstract int calculateTotalNoOfPapers();
	public abstract double calculateTotalCost();

}
