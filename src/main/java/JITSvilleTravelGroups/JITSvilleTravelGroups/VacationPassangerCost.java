package JITSvilleTravelGroups.JITSvilleTravelGroups;

public class VacationPassangerCost implements PassangerCategory{

	double rateFactor=0.5;
	private static int finalFair;
	
	public double calculateFair(VacationPassanger passangerInfo) {
		int miles=passangerInfo.getMiles();
		
		double totalFairPerPassanger=rateFactor*miles;
		
		finalFair+=totalFairPerPassanger;
		
		return totalFairPerPassanger;
	}

	public double calculateFair() {
		return finalFair;
	}
}
