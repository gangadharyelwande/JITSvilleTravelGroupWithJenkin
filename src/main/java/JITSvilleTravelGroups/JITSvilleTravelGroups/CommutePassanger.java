package JITSvilleTravelGroups.JITSvilleTravelGroups;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class CommutePassanger implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int stops;
	private boolean frequentCard;
	private boolean newsPaper;
	
	private int totalNoOfPapers;
	
	public CommutePassanger() {
		
	}
	
	public CommutePassanger(int stops, boolean frequentCard, boolean newsPaper) {
		if(stops<0) {
			throw new IllegalArgumentException("Negative stops are not allowed");
		}
		 this.stops=stops;
		 this.frequentCard=frequentCard;
		 this.newsPaper=newsPaper;
		 
		 if(newsPaper) {
			 totalNoOfPapers=totalNoOfPapers+1;
		 }
	}
	
	public int getPaperCount() {
		return totalNoOfPapers;
	}
	
	/**
	 * @return the commutePassangerList
	 */
	public List<CommutePassanger> getCommutePassangerList() {
		return commutePassangerList;
	}

	/**
	 * @param commutePassangerList the commutePassangerList to set
	 */
	public void setCommutePassangerList(List<CommutePassanger> commutePassangerList) {
		this.commutePassangerList = commutePassangerList;
	}


	private List<CommutePassanger> commutePassangerList= new LinkedList<CommutePassanger>();
	
	public void addCommutePassangerDetails(CommutePassanger commutePassanger) {
		this.commutePassangerList.add(commutePassanger);
	}
	
	/**
	 * @return the stops
	 */
	public int getStops() {
		return stops;
	}
	/**
	 * @param stops the stops to set
	 */
	public void setStops(int stops) {
		this.stops = stops;
	}
	/**
	 * @return the frequentCard
	 */
	public boolean isFrequentCard() {
		return frequentCard;
	}
	/**
	 * @param frequentCard the frequentCard to set
	 */
	public void setFrequentCard(boolean frequentCard) {
		this.frequentCard = frequentCard;
	}
	/**
	 * @return the newsPaper
	 */
	public boolean isNewsPaper() {
		return newsPaper;
	}
	/**
	 * @param newsPaper the newsPaper to set
	 */
	public void setNewsPaper(boolean newsPaper) {
		this.newsPaper = newsPaper;
	}
	
    	

}
